# Charter #

* Transform HTML tables into Google Charts.

## What does it do? ###

* Generates a Google Charts from tables on a page.
* Adds a toggle to allow users to still view the data as a table.
* Provides a url where users can save the generated .svg.
* Displays data in one of six charts types and a range of themes.
* Can run on one, many or all of the tables on a page.
* Every chart on the page can have a different type and/or style.

## Usage? ##

* Charter requires jQuery.
* Include the single script of any page you'd like it to run.
* Create one or more tables on your page.
* Table format:
  * At least two rows.
  * First row must be headers.
  * Two columns for Bar, Column & Pie.
  * Multiple columns for Stacked.
  * See below for a simple example.
* Add the chart type class to any of your tables (bar, pie, stacked or column)
* Add a second class to each table to specify your theme (see below).
* Optionally add an overall width for the chart.
* Sit back and refresh.

## Themes ##

Currently Charter has 16 themes consisting of 6 colours that will repeat from the start if a chart needs more. Three varieties of theme are included; solid colours, tints of colours and multi-coloured schemes (better for stacked and pie charts). Include one of of the following classes after your chart type class on the table:
* orange
* brown
* purple
* blue
* purple-b
* green
* orange-t
* brown-t
* purple-t
* blue-t
* purple-b-t
* green-t
* scheme-a
* scheme-b
* scheme-c
* scheme-d

## Table format ##

The following markup would generate a bar chart with a solid purple colour scheme. The markup will also work for column and pie charts. Charts with a stacked style require additional columns.

```
#!html

<table border="1" cellpadding="1" cellspacing="1" class="bar purple-b" style="width:500px;">
  <thead>
    <tr>
      <th scope="col">Year</th>
      <th scope="col">People</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>2016</td>
      <td>602</td>
    </tr>
    <tr>
      <td>2017</td>
      <td>615</td>
    </tr>
    <tr>
      <td>2018</td>
      <td>621</td>
    </tr>
  </tbody>
</table>

```

## What's next? ###

* The ability to add custom themes (somehow) without changing the source file.
* Some hardening for varying formatting and classes within tables.
* Options such as what to do on different screen sizes.
* Some solid testing and fall backs.

## Issues ##

* Add them [here](https://bitbucket.org/thecraighammond/charter/issues).

## Who do I talk to? ##

* @thecraighammond