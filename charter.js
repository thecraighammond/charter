(function() {
  var $, drawChart, positionAxisLabels, toggleTable;
    
    const TYPE_STACKED_CLASS = 'stacked';
    const TYPE_BAR_CLASS = 'bar';
    const TYPE_COLUMN_CLASS = 'column';
    const TYPE_PIE_CLASS = 'pie';
    const TYPE_GROUPEDBAR_CLASS = 'groupedbar';
    const TYPE_GROUPEDCOLUMN_CLASS = 'groupedcolumn';

    const availableChartTypes = [TYPE_STACKED_CLASS, TYPE_BAR_CLASS, TYPE_COLUMN_CLASS, TYPE_PIE_CLASS, TYPE_GROUPEDBAR_CLASS, TYPE_GROUPEDCOLUMN_CLASS];

  $ = jQuery;

  drawChart = function(containerId, chartType, colourScheme) {
    var annotatedRows, c, chart, colours, data, hTitle, headers, headersCount, i, legendStatus, options, seriesStyle, slicesStyle, stackedStatus, tableCols, tableRowData, tableRows, tableTitle, tableWidth, vTitle;
    tableRows = [];
    tableCols = [];
    tableTitle = $('#' + containerId + ' table').find('caption').html();
    tableTitle = '';
    tableWidth = $('#' + containerId + ' table').width();
    $('#' + containerId + ' tr').each(function() {
      var col, row, tableColData, tableRowData;
      row = [];
      col = [];
      tableRowData = $(this).find('td');
      tableColData = $(this).find('th');
      if (tableRowData.length > 0) {
        tableRowData.each(function() {
          row.push($(this).text());
        });
        tableRows.push(row);
      }
      if (tableColData.length > 0) {
        tableColData.each(function() {
          col.push($(this).text());
        });
        tableCols.push(col);
      }
    });
    tableRowData = [];
    tableRowData[0] = [
      tableCols[0][0], tableCols[0][1], {
        role: 'style'
      }
    ];
    switch (colourScheme) {
      case 'orange':
        colours = ['#e65c2b', '#e65c2b', '#e65c2b', '#e65c2b', '#e65c2b', '#e65c2b'];
        break;
      case 'brown':
        colours = ['#8a713b', '#8a713b', '#8a713b', '#8a713b', '#8a713b', '#8a713b'];
        break;
      case 'purple':
        colours = ['#71679b', '#71679b', '#71679b', '#71679b', '#71679b', '#71679b'];
        break;
      case 'blue':
        colours = ['#54779d', '#54779d', '#54779d', '#54779d', '#54779d', '#54779d'];
        break;
      case 'purple-b':
        colours = ['#87508c', '#87508c', '#87508c', '#87508c', '#87508c', '#87508c'];
        break;
      case 'green':
        colours = ['#7a7243', '#7a7243', '#7a7243', '#7a7243', '#7a7243', '#7a7243'];
        break;
      case 'orange-t':
        colours = ['#e65c2b', '#cd5226', '#b34822', '#9a3e1d', '#8a371a', '#732e15'];
        break;
      case 'brown-t':
        colours = ['#ad9056', '#9a804d', '#877043', '#74613a', '#685634', '#56482b'];
        break;
      case 'purple-t':
        colours = ['#877fa8', '#787196', '#696383', '#5b5571', '#514c65', '#433f54'];
        break;
      case 'blue-t':
        colours = ['#688eb3', '#5d7e9f', '#516f8c', '#465f78', '#3e556b', '#344759'];
        break;
      case 'purple-b-t':
        colours = ['#916c95', '#816085', '#715474', '#614864', '#574159', '#48364a'];
        break;
      case 'green-t':
        colours = ['#9c9561', '#8b8556', '#7a744c', '#696441', '#5e593a', '#4e4a30'];
        break;
      case 'scheme-a':
        colours = ['#688eb5', '#ad9056', '#e65c2b', '#75685f', '#867ea7', '#7a222e'];
        break;
      case 'scheme-b':
        colours = ['#916b95', '#688eb3', '#ad9056', '#e65c2b', '#7a7243', '#7a222e'];
        break;
      case 'scheme-c':
        colours = ['#e65c2b', '#ad9056', '#756854', '#688eb5', '#877ea7', '#7a222e'];
        break;
      case 'scheme-d':
        colours = ['#916a96', '#688eb3', '#ad9056', '#e65c2b', '#7a7243', '#7a222e'];
        break;
      default:
        colours = ['#e65c2b', '#e65c2b', '#e65c2b', '#e65c2b', '#e65c2b', '#e65c2b'];
    }
    stackedStatus = true;
    slicesStyle = [];
    seriesStyle = [];
    legendStatus = 'none';
    i = 0;
    headers = [];
    headersCount = tableCols[0].length;
    vTitle = tableCols[0][0];
    hTitle = tableCols[0][1];
    if (chartType === TYPE_STACKED_CLASS || chartType === TYPE_GROUPEDBAR_CLASS || chartType === TYPE_GROUPEDCOLUMN_CLASS) {
      legendStatus = 'right';
      hTitle = '';
      headers[0] = tableCols[0][0];
      i++;
      c = i;
      while (i < ((headersCount * 2) - 1)) {
        headers[i] = tableCols[0][c];
        headers[i + 1] = {
          role: 'annotation'
        };
        i = i + 2;
        c++;
      }
      tableRowData[0] = headers;
      annotatedRows = [];
      tableRows.forEach(function(data, index) {
        var newSet, set;
        set = data;
        newSet = [];
        i = c = 1;
        set.forEach(function(data, index) {
          newSet[0] = set[0];
          if (index > 0) {
            newSet[i] = set[c];
            newSet[i + 1] = set[c];
            i = i + 2;
            return c++;
          }
        });
        annotatedRows.push(newSet);
      });
      annotatedRows.forEach(function(data, index) {
        var currentRow;
        currentRow = [];
        annotatedRows[index].forEach(function(data, index) {
          seriesStyle[index] = {
            color: colours[index % 6]
          };
          if (index === 0) {
            currentRow[index] = data;
          } else {
            currentRow[index] = parseInt(data);
          }
        });
        tableRowData[index + 1] = currentRow;
      });
    } else {
      stackedStatus = false;
      tableRows.forEach(function(data, index) {
        if (chartType === 'pie') {
          legendStatus = 'right';
          slicesStyle[index] = {
            offset: 0.005,
            color: colours[index % 6]
          };
        }
        tableRowData[index + 1] = [data[0], parseInt(data[1]), colours[index % 6]];
      });
    }
    if (chartType === TYPE_COLUMN_CLASS || chartType === TYPE_GROUPEDCOLUMN_CLASS) {
      vTitle = tableRowData[0][1];
      hTitle = tableRowData[0][0];
    }
    if (chartType === TYPE_GROUPEDBAR_CLASS || chartType === TYPE_GROUPEDCOLUMN_CLASS) {
        stackedStatus = false;
    }

    data = google.visualization.arrayToDataTable(tableRowData);

    options = {
      title: tableTitle,
      chartArea: {
        height: '60%',
        width: tableWidth
      },
      isStacked: stackedStatus,
      pieStartAngle: 0,
      pieSliceTextStyle: {
        fontSize: 19,
        bold: true
      },
      legend: {
        position: legendStatus,
        textStyle: {
          fontSize: 16,
          bold: false
        }
      },
      fontName: 'Open Sans',
      slices: slicesStyle,
      series: seriesStyle,
      bar: {
        groupWidth: '45%'
      },
      hAxis: {
        title: hTitle,
        titleTextStyle: {
          italic: false,
          fontSize: 18
        }
      },
      vAxis: {
        title: vTitle,
        titleTextStyle: {
          italic: false,
          fontSize: 18
        }
      }
    };
    if (chartType === TYPE_PIE_CLASS) {
      chart = new google.visualization.PieChart(document.getElementById(containerId));
    }
    if (chartType === TYPE_BAR_CLASS) {
      chart = new google.visualization.BarChart(document.getElementById(containerId));
    }
    if (chartType === TYPE_COLUMN_CLASS) {
      chart = new google.visualization.ColumnChart(document.getElementById(containerId));
    }
    if (chartType === TYPE_STACKED_CLASS) {
      chart = new google.visualization.BarChart(document.getElementById(containerId));
    }
    if (chartType === TYPE_GROUPEDBAR_CLASS) {
      chart = new google.visualization.BarChart(document.getElementById(containerId));
    }
      if (chartType === TYPE_GROUPEDCOLUMN_CLASS) {
          chart = new google.visualization.ColumnChart(document.getElementById(containerId));
      }
    chart.draw(data, options);
    setTimeout(function() {
      return $('#' + containerId).find('svg').each(function() {
        var id = '#' + containerId + Math.random();
        $(this).addClass(chartType);
        $(this).attr('id', id)
        var s = new XMLSerializer().serializeToString(document.getElementById(id));
        if(!s.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)){
          s = s.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
        }
        if(!s.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)){
          s = s.replace(/^<svg/, '<svg xmlns:xlink="http://www.w3.org/1999/xlink"');
        }
        s = '<?xml version="1.0" standalone="no"?>\r\n' + s;
        var url = "data:image/svg+xml;charset=utf-8," + encodeURIComponent(s);
        positionAxisLabels($(this), hTitle);
        return $('#' + containerId).append(
            '<button style="position:absolute;right:0;bottom:-50px;padding:10px;font-size:16px;color:#111;background:transparent;z-index:99;" class="js-charter-show-table" data-table="' + containerId + '">Show data as table</button>' +
            '<a style="position:absolute;left:0;bottom:-20px;z-index:99;" href="' + url + '">Right click and Save link as... to download</a>'
        );
      });
    }, 0);
  };

  positionAxisLabels = function(svg, columnHeader) {
    var growAmount, horAxisLabel, horAxisLabelYPos, newHeight;
    //svg.find('text').removeAttr('transform');
    if ((svg.hasClass(TYPE_COLUMN_CLASS)) || (svg.hasClass(TYPE_BAR_CLASS))) {
      growAmount = 30;
      horAxisLabel = svg.find('text').filter(function() {
        return $(this).text() === columnHeader;
      });
      horAxisLabelYPos = parseInt(horAxisLabel.attr('y')) + growAmount;
      newHeight = parseInt(svg.attr('height')) + growAmount;
      svg.attr('height', newHeight);
      svg.closest('rect').attr('height', newHeight);
      horAxisLabel.attr('y', horAxisLabelYPos);
    }
  };

  toggleTable = function(button, containerId) {
    var table, tableVisible;
    tableVisible = $('#' + containerId).data('table-visible');
    table = $('#' + containerId).find('table');
    if (!tableVisible || tableVisible === void 0) {
      $('#' + containerId).data('table-visible', true);
      button.html('Show data as chart');
      $(table).parent().siblings('svg').hide();
      $(table).parent().css({
        'background': '#fff',
        'height': 'auto',
        'left': '50%',
        'top': 0,
        'transform': 'translateX(-50%)',
        'width': 'auto'
      });
    } else {
      $('#' + containerId).data('table-visible', false);
      button.html('Show data as table');
      $(table).parent().siblings('svg').show();
      $(table).parent().css({
        'height': '1px',
        'left': '-10000px',
        'top': '-9999px',
        'width': '1px'
      });
    }
  };

  $(function() {
    google.charts.load('current', {
      'packages': ['corechart']
    });
  });

  $(window).on('load', function() {
      var colourScheme, tableIndex, allowedChartTypes;
      tableIndex = 0;
      colourScheme = 'orange-t';
      allowedChartTypes = [TYPE_STACKED_CLASS, TYPE_BAR_CLASS, TYPE_COLUMN_CLASS, TYPE_PIE_CLASS, TYPE_GROUPEDBAR_CLASS, TYPE_GROUPEDCOLUMN_CLASS];
      $('table').each(function() {
          var chartClasses, chartType, containerId;
          if ($(this).attr('class') && $(window).width() > 482) {
              containerId = 'chart-container-' + tableIndex;
              chartClasses = $(this).attr('class').split(' ');
              $.each(chartClasses, function(i, v) {
                  if ($.inArray(v, availableChartTypes) !== -1) {
                      chartType = chartClasses[i];
                  }
                  else {
                      colourScheme = chartClasses[i];
                  }
              })
              if (allowedChartTypes.includes(chartType)) {
                  $(this).wrap('<div style="position:relative" class="chart-container" id="' + containerId + '"></div>');
                  google.charts.setOnLoadCallback(drawChart(containerId, chartType, colourScheme));
              }
              tableIndex++;
          }
      });
    $('.chart-container').on('click', '.js-charter-show-table', function() {
      toggleTable($(this), $(this).data('table'));
    });
  });

}).call(this);

//# sourceMappingURL=charter.js.map
